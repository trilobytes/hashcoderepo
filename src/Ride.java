

public class Ride {
    public static int bonus;
    public int id;
    public int startx;
    public int starty;
    public int endx;
    public int endy;
    public int earliest;
    public int latest;

    public Ride(int id,int startx, int starty, int endx, int endy, int earliest, int latest) {
        this.id = id;
        this.startx = startx;
        this.starty = starty;
        this.endx = endx;
        this.endy = endy;
        this.earliest = earliest;
        this.latest = latest;
    }

    public double score(int x, int y, int t){
        int bonus2 = (Main.distance(x,y,startx,starty) +t <= earliest)? bonus:0;
        int distance = (Main.distance(x,y,startx,startx) + Main.distance(startx,starty,endx,endy) + t <= latest)? Main.distance(startx,starty,endx,endy) :0;

        return (distance+ Main.distance(startx,starty,endx,endy))/((double)(Integer.max(earliest,t+Main.distance(x,y,startx,starty)))+distance+1)+bonus2*Math.log(bonus);
    }
    public boolean possible(int x, int y, int t){
        return Main.distance(x,y,startx,startx) + Main.distance(startx,starty,endx,endy) + t <= latest;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Ride ride = (Ride) o;

        return id == ride.id;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public String toString() {

        return "Ride{" +
                "startx=" + startx +
                ", starty=" + starty +
                ", endx=" + endx +
                ", endy=" + endy +
                ", earliest=" + earliest +
                ", latest=" + latest +
                '}';
    }
}
