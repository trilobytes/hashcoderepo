import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

import static java.lang.Integer.max;
import static java.lang.Math.abs;

public class Main {

    public static int distance(int startx, int starty, int endx, int endy){
        return abs(startx - endx)+abs(starty-endy);
    }


    public static void main(String[] args) {
        String file = "e";
        String filename = file+".in";
        Scanner f = null;
        try {
            f = new Scanner(new FileReader(filename));
        } catch (IOException e) {
            System.out.println(e.getLocalizedMessage());
        }

        int R = f.nextInt();
        int C = f.nextInt();
        int F = f.nextInt();
        int N = f.nextInt();
        int B = f.nextInt();
        int T = f.nextInt();
        Ride.bonus = B;
        List<Ride> rides = new ArrayList<>(N);
        for (int i=0; i<N;i++){
            int a = f.nextInt();
            int b = f.nextInt();
            int x = f.nextInt();
            int y = f.nextInt();
            int s = f.nextInt();
            int f1 = f.nextInt();
            rides.add(new Ride(i,a,b,x,y,s,f1));
        }
        f.close();
        List<Vehicle> vehicles = new ArrayList<>(F);
        for (int i = 0; i<F;i++){
            vehicles.add(new Vehicle(0,0,true));
        }

        for (int i = 0; i<F; i++) {
            Vehicle v = vehicles.get(i);
            PriorityQueue<Ride> doable;
            do {
                doable = new PriorityQueue<>(1, (o1, o2) -> {
                    if (o1.score(v.x,v.y,v.t) == o2.score(v.x,v.y,v.t))
                        return 0;
                    if (o1.score(v.x,v.y,v.t) > o2.score(v.x,v.y,v.t))
                        return -1;
                    return 1;

                });
                for (int j = 0; j < N; j++) {
                    Ride r = rides.get(j);
                    if (r.possible(v.x, v.y, v.t)) {
                        doable.add(r);
                    }
                }
                if (doable.isEmpty()) {
                    continue;
                }
                v.rides.add(doable.peek());
                rides.remove(doable.peek());
                Ride r = doable.peek();
                v.t = v.t + max(r.earliest, distance(v.x,v.y,r.startx,r.starty)) + distance(r.startx, r.starty, r.endx, r.endy);
                N--;
            } while (v.t < T && doable.size() != 0);
        }

        PrintWriter p = null;
        try {
            p = new PrintWriter(file+".out");
        } catch (IOException e) {
            System.out.println(e.getLocalizedMessage());
        }
        for (Vehicle v : vehicles){
            p.println(v.toString());
        }

        p.close();

    }
}
