import java.util.ArrayList;
import java.util.List;

public class Vehicle {
    public int x;
    public int y;
    public boolean busy;
    public int t;
    public List<Ride> rides = new ArrayList<>();
    public Vehicle(int x, int y, boolean busy) {
        this.x = x;
        this.y = y;
        this.busy = busy;
    }


    @Override
    public String toString() {
        String s = "";
        for (Ride r : rides){
            s+=r.id+" ";
        }
        return rides.size() + " "+s;
    }

}
